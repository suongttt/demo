import React, { useState, useEffect } from "react";
import Axios from "axios";
interface User {
  name?: string;
  birthday?: any;
  tel?: string;
  email?: string;
  role?: string;
}
export default function UpdateUser(props: any) {
  const [state, setState] = useState({
    name: "",
    birthday: "",
    tel: "",
    email: "",
    role: "",
    idrole: "",
    listrole: [],
    priuser: {},
  });
  useEffect(() => {
    Axios.get("http://localhost:9000/api/users/" + props.match.params.id).then(
      (res) => {
        const user = res.data;
        setState((s) => ({
          ...s,
          name: user.name,
          birthday: user.birthday,
          tel: user.tel,
          email: user.email,
          role: user.role.name,
          idrole: user.role._id,
          priuser: user,
        }));
      }
    );
    Axios.get("http://localhost:9000/api/roles")
      .then((res) => {
        setState((s) => ({ ...s, listrole: res.data }));
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  const onchange = (e: any) => {
    let target = e.target;
    let name = target.name;
    let value = target.value;
    setState((s) => ({ ...s, [name]: value }));
  };
  const onSubmit = (e: any) => {
    let updateuser: User = {};
    const priuser: User = state.priuser;
    if (state.name !== priuser.name) {
      updateuser.name = state.name;
    }
    if (state.birthday !== priuser.birthday) {
      updateuser.birthday = state.birthday;
    }
    if (state.email !== priuser.email) {
      updateuser.email = state.email;
    }
    if (state.tel !== priuser.tel) {
      updateuser.tel = state.tel;
    }
    if (state.role !== "Admin" && state.role !== "Guest") {
      updateuser.role = state.role;
    }
    console.log("updatauser :>> ", updateuser);
    e.preventDefault();
    Axios.put(
      "http://localhost:9000/api/users/" + props.match.params.id,
      updateuser
    ).then((res) => console.log(res.data));
  };

  return (
    <div>
      <h3>Edit User</h3>
      <form onSubmit={onSubmit}>
        <div>
          <label>Name:</label>
          <input
            type="text"
            name="name"
            className="form-control"
            value={state.name}
            onChange={onchange}
          />
        </div>
        <div>
          <label>Birthday:</label>
          <div>
            <input
              type="text"
              name="birthday"
              className="form-control"
              value={state.birthday}
              onChange={onchange}
            />
          </div>
        </div>
        <div>
          <label>Telephone:</label>
          <input
            type="text"
            name="tel"
            className="form-control"
            value={state.tel}
            onChange={onchange}
          />
        </div>
        <div>
          <label>Email:</label>
          <input
            type="text"
            name="email"
            className="form-control"
            value={state.email}
            onChange={onchange}
          />
        </div>
        <div>
          <label>Role:</label>
          <select
            name="role"
            required
            className="form-control"
            value={state.role}
            onChange={onchange}
          >
            <option value={state.idrole}>{state.role}</option>
            {state.listrole.map((role: any, index: any) => {
              if (role.name !== state.role) {
                const r = role;
                return <option value={r._id}>{r.name}</option>;
              }
            })}
          </select>
        </div>
        <div className="form-group">
          <input type="submit" value="Edit User" className="btn btn-primary" />
        </div>
      </form>
    </div>
  );
}
