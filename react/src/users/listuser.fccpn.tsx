import React, { useState, useEffect } from "react";
import axios from "axios";
import { Link } from "react-router-dom";

interface User {
  _id: string;
  name: string;
  birthday: string;
  tel: string;
  email: string;
  role: Role;
}
interface Role {
  _id: string;
  name: string;
}
export default function UserList() {
  const [state, setState] = useState({
    userList: [],
    user: false,
  });
  useEffect(() => {
    axios
      .get("http://localhost:9000/api/users")
      .then((res) => {
        setState((s) => ({ ...s, userList: res.data }));
      })
      .catch((err) => {
        console.log("err mess", err);
      });
  }, []);

  const deleteUser = (event: any, id: string) => {
    axios.delete("http://localhost:9000/api/users/" + id).then((res) => {
      const { userList } = state;
      const filteredList = userList.filter((u: User) => u._id !== id);
      setState((s) => ({ ...s, userList: filteredList }));
    });
  };

  return (
    <div className="container">
      <h3>List User</h3>
      <table className="table">
        <thead className="thead-light">
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Birthday</th>
            <th>Tel</th>
            <th>Email</th>
            <th>Role</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {state.userList?.map((user: User, index) => {
            return (
              <tr>
                <td>{index + 1}</td>
                <td>{user.name}</td>
                <td>{user.birthday}</td>
                <td>{user.tel}</td>
                <td>{user.email}</td>
                <td>{user.role.name}</td>
                <td>
                  <Link to={"/users/update/" + user._id}>edit</Link> |
                  <a href="#" onClick={(e) => deleteUser(e, user._id)}>
                    delete
                  </a>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}
