import React, { Component } from "react";
import axios from "axios";
import { RouteComponentProps } from "react-router-dom";

type IdParams = {
  id: string;
};
interface UserProps extends RouteComponentProps<IdParams> {}
interface UpdateUserState {
  name?: string;
  birthday?: any;
  tel?: string;
  email?: string;
  roles?: any;
  role?: string;
  id?: string;
}

export default class UpdateUser extends React.Component<
  UserProps,
  UpdateUserState
> {
  constructor(props: any) {
    super(props);
    console.log("props", props);
    this.state = {
      name: "",
      birthday: "",
      tel: "",
      email: "",
      roles: [],
      role: "",
    };
  }

  componentDidMount() {
    console.log("this.props", this.props);
    axios
      .get("http://localhost:9000/api/users/" + this.props.match.params.id)
      .then((res) => {
        this.setState({
          name: res.data.name,
          birthday: res.data.birthday,
          tel: res.data.tel,
          email: res.data.email,
          roles: ["Admin", "Guest"],
          role: res.data.role.name,
        });
      });
  }
  // onChangeRole = (e: any) => {
  //   let r_id = "5ed9aac793ef44c4a716aac4";
  //   if (e.target.value) {
  //     r_id = "5ed9aad093ef44c4a716aac5";
  //   }
  //   this.setState({ role: r_id });
  // };
  onSubmit = (e: any) => {
    e.preventDefault();
    this.state.role === "Admin"
      ? this.setState({ role: "5ed9aad093ef44c4a716aac5" })
      : this.setState({ role: "5ed9aac793ef44c4a716aac4" });
    console.log(this.state.role);
    axios
      .put(
        "http://localhost:9000/api/users/" + this.props.match.params.id,
        this.state
      )
      .then((res) => console.log(res.data));
  };
  render() {
    console.log("render :>> ");
    return (
      <div>
        <h3>Edit User</h3>
        <form onSubmit={this.onSubmit}>
          <div>
            <label>Name:</label>
            <input
              type="text"
              className="form-control"
              value={this.state.name}
              onChange={(e) => {
                this.setState({ name: e.target.value });
              }}
            />
          </div>
          <div>
            <label>Birthday:</label>
            <div>
              <input
                type="text"
                className="form-control"
                value={this.state.birthday}
                onChange={(e) => this.setState({ birthday: e.target.value })}
              />
            </div>
          </div>
          <div>
            <label>Telephone:</label>
            <input
              type="text"
              onChange={(e) => this.setState({ tel: e.target.value })}
              className="form-control"
              value={this.state.tel}
            />
          </div>
          <div>
            <label>Email:</label>
            <input
              type="text"
              onChange={(e) => this.setState({ email: e.target.value })}
              className="form-control"
              value={this.state.email}
            />
          </div>
          <div>
            <label>Role:</label>
          </div>
          <div className="form-group">
            <select
              name="selectRole"
              required
              className="form-control"
              value={this.state.role}
              onChange={(e) => this.setState({ role: e.target.value })}
            >
              {this.state.roles.map((user: any, idUser: any) => {
                return (
                  <option key={idUser} value={user}>
                    {user}
                  </option>
                );
              })}
            </select>
          </div>
          <div className="form-group">
            <input
              type="submit"
              value="Edit User"
              className="btn btn-primary"
            />
          </div>
        </form>
      </div>
    );
  }
}
