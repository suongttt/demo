import React, { useState, useEffect } from "react";
import axios from "axios";
interface Role {
  _id: string;
  name: string;
}
export default function RoleList() {
  const [state, setState] = useState({
    rolelist: [],
  });
  useEffect(() => {
    axios
      .get("http://localhost:9000/api/roles")
      .then((res) => {
        const datarole = res.data;
        setState((s) => ({ ...s, rolelist: datarole }));
      })
      .catch((err) => {
        console.log("err mess", err);
      });
  }, []);
  return (
    <div className="container">
      <h3>List Role</h3>
      <table className="table">
        <thead className="thead-light">
          <tr>
            <th> #</th>
            <th>Role</th>
          </tr>
        </thead>
        <tbody>
          {state.rolelist.map((role: Role, index) => {
            return (
              <tr>
                <td>{index + 1}</td>
                <td>{role.name}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}
