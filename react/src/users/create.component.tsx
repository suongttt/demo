import React, { Component, Props } from "react";
import axios from "axios";
import { ByRoleOptions } from "@testing-library/react";

interface CreateUserProps {
  //name?: string;
}
interface CreateUserState {
  name?: string;
  birthday?: any;
  tel?: string;
  email?: string;
  role?: string;
  listrole: any;
}

export default class CreateUser extends React.Component<
  CreateUserProps,
  CreateUserState
> {
  constructor(props: CreateUserProps) {
    super(props);
    this.state = {
      name: "",
      birthday: "",
      tel: "",
      email: "",
      role: "",
      listrole: [],
    };
  }
  componentDidMount() {
    axios
      .get("http://localhost:9000/api/roles")
      .then((res) => {
        if (res.data.length > 0) {
          this.setState({
            listrole: res.data,
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }
  createrole() {
    console.log(this.state.role);
    if (this.state.role == this.state.listrole[1].name) {
      this.setState({ role: this.state.listrole[1]._id });
    }
    if (this.state.role == this.state.listrole[0].name) {
      this.setState({ role: this.state.listrole[0]._id });
    }
  }
  onSubmit = (e: any) => {
    e.preventDefault();
    // this.createrole;
    console.log("this.state.role :>> ", this.state);
    axios.post("http://localhost:9000/api/users/", this.state).then((res) => {
      console.log(res.data);
    });
  };
  render() {
    return (
      <div>
        <h3>Create New User</h3>
        <form action="" onSubmit={this.onSubmit}>
          <div className="form-group">
            <label>Username:</label>
            <input
              name=""
              id=""
              ref="userInput"
              required
              className="form-control"
              value={this.state.name}
              onChange={(e) => this.setState({ name: e.target.value })}
              placeholder="Enter name..."
              type="text"
            />
          </div>
          <div className="form-group">
            <label>Birthday:</label>
            <div>
              <input
                type="text"
                required
                className="form-control"
                placeholder="Enter birthday..."
                value={this.state.birthday}
                onChange={(e) => this.setState({ birthday: e.target.value })}
              />
            </div>
          </div>
          <div className="form-group">
            <label>Telephone:</label>
            <input
              type="text"
              required
              className="form-control"
              placeholder="Enter telephone..."
              value={this.state.tel}
              onChange={(e) => this.setState({ tel: e.target.value })}
            />
          </div>
          <div className="form-group">
            <label>Email:</label>
            <input
              type="text"
              required
              className="form-control"
              placeholder="Enter email..."
              value={this.state.email}
              onChange={(e) => this.setState({ email: e.target.value })}
            />
          </div>
          <div className="form-group">
            <label>Role:</label>
            <select
              name="roleSelect"
              required
              className="form-control"
              value={this.state.role}
              onChange={(e) => this.setState({ role: e.target.value })}
            >
              {this.state.listrole.map((role: any, index: any) => {
                return <option value={role._id}>{role.name}</option>;
              })}
            </select>
          </div>
          <div className="form-group">
            <input
              type="submit"
              value="Create User"
              className="btn btn-primary"
            />
          </div>
        </form>
      </div>
    );
  }
}
