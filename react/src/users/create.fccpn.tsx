import React, { useState, useEffect } from "react";
import Axios from "axios";
interface User {
  name?: string;
  birthday?: any;
  tel?: string;
  email?: string;
  role?: string;
}
export default function UpdateUser(props: any) {
  const [state, setState] = useState({
    name: "",
    birthday: "",
    tel: "",
    email: "",
    role: "",
    listrole: [],
  });
  useEffect(() => {
    Axios.get("http://localhost:9000/api/roles")
      .then((res) => {
        const listrole = res.data;
        setState((s) => ({
          ...s,
          listrole: res.data,
          role: listrole[0]._id,
        }));
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);
  const onchange = (e: any) => {
    let target = e.target;
    let name = target.name;
    let value = target.value;
    console.log("object :>> ", value);
    setState((s) => ({ ...s, [name]: value }));
  };

  const onSubmit = (e: any) => {
    const newuser: User = {};
    newuser.name = state.name;
    newuser.birthday = state.birthday;
    newuser.email = state.email;
    newuser.tel = state.tel;
    newuser.role = state.role;
    e.preventDefault();
    console.log("newuser", newuser);
    Axios.post("http://localhost:9000/api/users/", newuser).then((res) =>
      console.log(res.data)
    );
  };

  return (
    <div>
      <h3>Create User</h3>
      <form onSubmit={onSubmit}>
        <div>
          <label>Name:</label>
          <input
            type="text"
            name="name"
            required
            className="form-control"
            value={state.name}
            onChange={onchange}
          />
        </div>
        <div>
          <label>Birthday:</label>
          <div>
            <input
              type="text"
              name="birthday"
              required
              className="form-control"
              value={state.birthday}
              onChange={onchange}
            />
          </div>
        </div>
        <div>
          <label>Telephone:</label>
          <input
            type="text"
            name="tel"
            className="form-control"
            required
            value={state.tel}
            onChange={onchange}
          />
        </div>
        <div>
          <label>Email:</label>
          <input
            type="text"
            name="email"
            required
            className="form-control"
            value={state.email}
            onChange={onchange}
          />
        </div>
        <div>
          <label>Role:</label>
          <select
            name="role"
            required
            className="form-control"
            placeholder="Choose option"
            onChange={onchange}
          >
            {/* <option>Choose option</option> */}
            {state.listrole.map((role: any, index: any) => {
              return <option value={role._id}>{role.name}</option>;
            })}
          </select>
        </div>
        <div className="form-group">
          <input
            type="submit"
            value="Create User"
            className="btn btn-primary"
          />
        </div>
      </form>
    </div>
  );
}
