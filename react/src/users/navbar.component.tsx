import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import CreateUser from "./create.fccpn";
import UpdateUser from "./update.fccpn";
import User from "./listuser.fccpn";
import RoleList from "./listrole.component";

function Navbar() {
  return (
    <Router>
      <div>
        <nav className="navbar navbar-dark bg-dark navbar-expand-lg">
          <Link to="/" className="navbar-brand">
            HOME
          </Link>
          <div className="collpase navbar-collapse">
            <ul className="navbar-nav mr-auto">
              <li className="navbar-item">
                <Link to="/users" className="nav-link">
                  List User
                </Link>
              </li>
              <li className="navbar-item">
                <Link to="/users/create" className="nav-link">
                  Create User
                </Link>
              </li>
              <li className="navbar-item">
                <Link to="/roles" className="nav-link">
                  List Role
                </Link>
              </li>
            </ul>
          </div>
        </nav>
        <Switch>
          <Route exact path="/users" component={User} />
          <Route path="/users/create" component={CreateUser} />
          <Route path="/users/update/:id" component={UpdateUser} />
          <Route path="/roles" component={RoleList} />
        </Switch>
      </div>
    </Router>
  );
}
export default Navbar;
