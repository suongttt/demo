import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
interface ListUserProps {
  userProps?: any;
  deleteUser?: any;
}
interface LisetUserState {
  users?: any;
}
const UsersTable = (props: ListUserProps) => (
  <tr>
    <td>{props.userProps.name}</td>
    <td>{props.userProps.birthday}</td>
    <td>{props.userProps.tel}</td>
    <td>{props.userProps.email}</td>
    <td>{props.userProps.role.name}</td>
    <td>
      <Link to={"/update/" + props.userProps._id}>edit</Link> |
      <a
        href="#"
        onClick={() => {
          props.deleteUser(props.userProps._id);
        }}
      >
        delete
      </a>
    </td>
  </tr>
);
export default class UserList extends React.Component<
  ListUserProps,
  LisetUserState
  > {
  constructor(props: ListUserProps) {
    super(props);
    this.state = { users: [] };
  }
  componentDidMount() {
    axios
      .get("http://localhost:9000/api/users")
      .then((res) => {
        this.setState({ users: res.data });
      })
      .catch((error) => {
        console.log(error);
      });
  }
  deleteUser = (id: any) => {
    axios.delete("http://localhost:9000/api/users/" + id).then((res) => {
      console.log(res.data);
      this.setState({
        users: this.state.users.filter((el: { _id: any }) => el._id !== id),
      });
    });
  };
  usersList = () => {
    return this.state.users.map((currentuser: any) => {
      console.log("cusrrentuser", currentuser.name, currentuser.birthday);

      return (
        <UsersTable
          userProps={currentuser}
          deleteUser={this.deleteUser}
          key={currentuser._id}
        />
      );
    });
  };
  render() {
    return (
      <div>
        <h3>List User</h3>
        <table className="table">
          <thead className="thead-light">
            <tr>
              <th>Name</th>
              <th>Birthday</th>
              <th>Tel</th>
              <th>Email</th>
              <th>Role</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>{this.usersList()}</tbody>
        </table>
      </div>
    );
  }
}
