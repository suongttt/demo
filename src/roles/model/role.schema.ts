import { Schema, Prop, SchemaFactory } from "@nestjs/mongoose";

import * as mongoose from 'mongoose';
@Schema()
export class Role extends mongoose.Document{
    @Prop()
    name: string;
}

export const roleSchema = SchemaFactory.createForClass(Role)