import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { roleSchema } from "./model/role.schema";

import { RolesController } from "../roles/role.controller";
import { RoleService } from "../roles/role.service";
@Module({
    imports: [MongooseModule.forFeature([{name: 'roles', schema: roleSchema}])],
    controllers: [RolesController],
    providers: [RoleService],
})
export class RolesModule{}