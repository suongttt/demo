import { Controller, Get, Res } from "@nestjs/common";
import { RoleService } from "./role.service";

@Controller('api/roles')
export class RolesController {
    constructor(private readonly roleSevice: RoleService){}
    @Get()
    async getListRoles(@Res() res){
        const roles= await this.roleSevice.getListroles();
        res.status((roles) ? 200 : 400).json(roles);
    }
}