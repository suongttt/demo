import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Role } from './model/role.schema';

@Injectable()
export class RoleService {
    constructor(@InjectModel('roles') private readonly roleModel: Model<Role>){}
    async getListroles() {
        const roles= await this.roleModel.find({});
        return roles
     }
}
