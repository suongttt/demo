import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './users/users.module';
import { RolesModule } from './roles/role.module';
@Module({
  imports: [
    UserModule,
    RolesModule,
    MongooseModule.forRoot(
      'mongodb://suong-tran:suong-tran@54.255.220.225/suong-tran',
    ),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
