import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";

import { UsersController } from "./user.controller";
import { UsersService } from "./users.service";
import { userSchema } from "./model/user.schema";
import { IsUserAlreadyExistConstraint } from "./validator/validate.emailused";

@Module({
    imports: [MongooseModule.forFeature([{name: 'users', schema: userSchema}])],
    controllers: [UsersController],
    providers: [UsersService,IsUserAlreadyExistConstraint],
})
export class UserModule{}