import {
  registerDecorator,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
} from 'class-validator';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User } from '../model/user.schema';
import { Injectable } from '@nestjs/common';
import { Type } from 'class-transformer';
import { type } from 'os';
export function IsEmailUsed(validationOptions?: ValidationOptions) {
  return function(object: Object, propertyName: string) {
    registerDecorator({
      name: 'IsEmailUsed',
      async: true,
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: IsUserAlreadyExistConstraint,
    });
  };
}

@ValidatorConstraint({ async: true })
@Injectable()
export class IsUserAlreadyExistConstraint
  implements ValidatorConstraintInterface {
  constructor(@InjectModel('users') private readonly userModel: Model<User>) {}

  validate(value: any, args: ValidationArguments) {
    return this.userModel.find({ email: value }).then(user => {
      if (user.length > 0) return false;
      return true;
    });
  }
}

export function MaxDateString(validationOptions?: ValidationOptions) {
  return function(object: Object, propertyName: string) {
    registerDecorator({
      name: 'MaxDateString',
      async: true,
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: MaxDateConstraint,
    });
  };
}

@ValidatorConstraint({ async: true })
@Injectable()
export class MaxDateConstraint implements ValidatorConstraintInterface {
  validate(value: string, args: ValidationArguments) {
    const date = new Date(value);
    const nowday = new Date();

    console.log('date', date);
    console.log('nowday', nowday);
    return date < nowday;
  }
}
