import { IsEmailUsed, MaxDateString } from './validate.emailused';
import {
  IsNotEmpty,
  IsEmail,
  MinLength,
  MaxLength,
  MaxDate,
  IsDate,
  IsOptional,
  IsMongoId,
  Matches,
  IsIn,
} from 'class-validator';
import { Types } from 'mongoose';
export class FindOneParams {
  @IsMongoId()
  id: string;
}
export class userValidatorCreate {
  @IsNotEmpty()
  @Matches(/^[a-zA-Z0-9]*$/, {
    message: 'Name should not contains special characters',
  })
  @MinLength(4)
  @MaxLength(20)
  name: string;

  @IsEmail()
  @IsEmailUsed({ message: 'Email already exists!' })
  email: string;

  @IsNotEmpty()
  @MaxDateString({ message: 'Are you from the future?' })
  @Matches(/^\d{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])$/, {
    message: 'invalid date format',
  })
  birthday: Date;

  @Matches(/^[a-zA-Z0-9]*$/, { message: 'Invalid phone number' })
  @MinLength(3)
  tel: string;

  @IsNotEmpty({ message: 'Role is require' })
  @IsMongoId()
  @IsIn(['5ed9aac793ef44c4a716aac4', '5ed9aad093ef44c4a716aac5'])
  role: Types.ObjectId;
}

export class userValidatorUpdate {
  @IsOptional()
  @Matches(/[a-zA-Z0-9]/, {
    message: 'Name should not contains special characters',
  })
  @MinLength(4)
  @MaxLength(50)
  name: string;

  @IsOptional()
  @IsEmail()
  email: string;

  @IsOptional()
  @MaxDateString({ message: 'Are you from the future?' })
  @Matches(/^\d{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])$/, {
    message: 'invalid date format',
  })
  birthday: string;

  @IsOptional()
  @MinLength(3)
  @Matches(/^0[0-9]/, { message: 'Invalid phone number' })
  tel: string;

  @IsOptional()
  @IsMongoId()
  @IsIn(['5ed9aac793ef44c4a716aac4', '5ed9aad093ef44c4a716aac5'])
  role: Types.ObjectId;
}
