import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Types } from 'mongoose';
import * as mongoose from 'mongoose';
@Schema()
export class User extends mongoose.Document {
  @Prop()
  name: string;

  @Prop()
  birthday: string;

  @Prop()
  tel: string;

  @Prop()
  email: string;

  @Prop({ type: Types.ObjectId, ref: 'roles' })
  role: Types.ObjectId;
}

export const userSchema = SchemaFactory.createForClass(User);
