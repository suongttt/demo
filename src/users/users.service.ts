import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User } from './model/user.schema';
import {
  userValidatorCreate,
  userValidatorUpdate,
} from './validator/user.validate';

@Injectable()
export class UsersService {
  constructor(@InjectModel('users') private readonly userModel: Model<User>) {}

  async createUser(user: userValidatorCreate) {
    const newUser = new this.userModel(user);
    return await newUser.save();
  }

  async getListUser() {
    return await this.userModel.find({}, { __v: 0 }).populate('role');
  }

  async getDetailUser(userId: string) {
    return await this.userModel
      .findById({ _id: userId }, { __v: 0 })
      .populate('role');
  }

  async updateUser(userId: string, user: userValidatorUpdate) {
    return await this.userModel
      .findByIdAndUpdate({ _id: userId }, user, { new: true })
      .populate('role');
  }

  async deleteUser(userId: string) {
    var deleteUser = await this.userModel.remove({ _id: userId });
    return deleteUser.deletedCount > 0 ? true : false;
  }
}
