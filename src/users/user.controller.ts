import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Put,
  Delete,
  Res,
  UsePipes,
  ValidationPipe,
  BadRequestException,
} from '@nestjs/common';
import { UsersService } from './users.service';
import {
  userValidatorCreate,
  FindOneParams,
  userValidatorUpdate,
} from './validator/user.validate';
import { MESSAGES } from '@nestjs/core/constants';

@Controller('api/users')
@UsePipes(
  new ValidationPipe({
    exceptionFactory: errors => {
      const messages = errors.map(err => {
        return { field: err.property, message: err.constraints };
      });
      throw new BadRequestException(messages);
    },
  }),
)
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post()
  async createUser(@Res() res, @Body() user: userValidatorCreate) {
    const newuser = await this.usersService.createUser(user);
    res.status(newuser ? 201 : 502).json(newuser);
  }

  @Get()
  async getListUser(@Res() res) {
    const users = await this.usersService.getListUser();
    res.status(users ? 200 : 502).json(users);
  }

  @Get(':id')
  async getDetailUser(@Res() res, @Param() params: FindOneParams) {
    const user = await this.usersService.getDetailUser(params.id);
    res.status(user ? 200 : 502).json(user);
  }

  @Put(':id')
  async updateUser(
    @Res() res,
    @Param() params: FindOneParams,
    @Body() user: userValidatorUpdate,
  ) {
    if (user.name || user.email || user.birthday || user.role || user.tel) {
      console.log('user :>> ', user);
      const userupdate = await this.usersService.updateUser(params.id, user);
      res.status(userupdate ? 200 : 502).json(userupdate);
    } else res.status(200).json({ message: 'no update value' });
  }
  @Delete(':id')
  async deleteUser(@Res() res, @Param() params: FindOneParams) {
    const deletedCount = await this.usersService.deleteUser(params.id);
    res.status(deletedCount ? 204 : 203).json(null);
  }
}
