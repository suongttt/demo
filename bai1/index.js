const express = require('express');
const mongoose= require('mongoose')
const userRouter = require('./routers/user.router');

const app = express();
var port=9000;

mongoose.connect('mongodb://localhost/excercise1');

app.set('view engine','pug');
app.set('views', './views');

app.use(express.json()) ;
app.use(express.urlencoded({ extended: true })) ;


app.get('/', (req, res) =>{
    res.send('<h1>Hello</h1> <a href="/api/users"> User List</a>');
});
app.use('/api/users',userRouter )

app.listen(port, () =>{
    console.log('Example app listening ' +port );
});