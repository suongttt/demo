const mongoose = require('mongoose');
var userSchema = new mongoose.Schema({
    Name: String,
    Birthday: Date,
    Tel: String,
    Email: String,
    Role: String,
  });
var User = mongoose.model('User', userSchema,'users');
module.exports = User;