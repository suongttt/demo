
const express=require('express');
const { check, validationResult } = require('express-validator');
// module.exports.checkUser =(req, res, next) =>{
//     var errors=[];
//     if(!req.body.Name){
//         errors.push('Name is required.')
//       }if(!req.body.Birthday){
//         errors.push('Birthday is required.');
//     }if(!req.body.Tel){
//         errors.push('Tel is required.');
//     }
//     if(!req.body.Email){
//         errors.push('Email is required.');
//     }
//     if(!req.body.Role){    
//         errors.push('Role is required.');
//     }
//     if(errors.length){
//     console.log( req.body);
//         res.render('create',{
//             errors: errors,
//             values: req.body
//     })
//     return ;
//     }
//     next();
// }
module.exports.checkUserinput =(req, res, next)=>{
    [
        check('req.body.Name', 'NAme does not Empty').not().isEmpty(),
        //check('Name', 'Name more than 6 degits').isLength({ min: 6 }),
        check('req.body.Email', 'Email does not Empty').not().isEmpty(),
        check('req.body.Email', 'Email not email').isEmail(),
        check('req.body.Birthday', 'Invalid birthday').isISO8601('yyyy-mm-dd'),
        check('req.body.Tel', 'Tel more than 10 degits').isLength({ min: 10 }),
        check('req.body.Role', 'Role does not Empty').not().isEmpty()
    ]
    const errors = validationResult(req);
    console.log(req.body)
    console.log((errors));
    if (!errors.isEmpty()) {
        res.render('create',{ 
            errors: errors.array(),
            values: req.body
        });
    return;
    }
    next();
}