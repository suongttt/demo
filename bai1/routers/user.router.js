const express=require('express');
const router = express.Router();

var User = require('../models/user.model');
var validate = require('../validators/user.validator');
const { check, validationResult } = require('express-validator');

//useList
router.get("/", async (req,res)=> {
    var users= await User.find();
    var page;
    (users.length%4==0)? page=users.length/4:page=users.length/4+1;
    var uerspage= await User.find().limit(4).skip((req.query.page-1)*4);
    var pagenumbers =[];
    for(var i=1; i<=page;i++){
        pagenumbers.push(i);
    }
    res.render('index',{
        users: uerspage,
        pagenumbers: pagenumbers
    })
});

router.get("/api/roles/ ", async (req, res) => {
    res.send('list roles');
});

router.get("/create", async (req, res) => {
    res.render('create');
});

//UserDetail
router.get("/:id", async (req,res)=>{
    const { id } = req.params;
    var user = await User.findById(id);
    getDegits = (number) => number <= 9 ? `0${number}` : number;
    //console.log(user);
    res.render('view',{
        user: user,
        getDate: `${user.Birthday.getFullYear()}-${getDegits(user.Birthday.getMonth() + 1)}-${getDegits(user.Birthday.getDate())}`
    })
})

//updateUser
router.get('/edit/:id', async (req,res)=>{
    const { id } = req.params;
    var user = await User.findById(id);
    getDegits = (number) => number <= 9 ? `0${number}` : number;
    res.render('edit',{
        user,
        getDate:`${user.Birthday.getFullYear()}-${getDegits(user.Birthday.getMonth() + 1)}-${getDegits(user.Birthday.getDate())}`
    });
})

router.put('/:id',[
    check('Name', 'NAme does not Empty').not().isEmpty(),
    check('Name', 'Name is Alpha').isAlpha(),
    check('Email', 'Email does not Empty').not().isEmpty(),
    check('Email', 'Email not email').isEmail(),
    check('Birthday', 'Invalid birthday').isISO8601('yyyy-mm-dd'),
    check('Tel', 'Tel more than 10 degits').isLength({ min: 10 }),
    check('Role', 'Role does not Empty').not().isEmpty()
  ], async (req,res)=>{
    const { id } = req.params;
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        var user = await User.findById(id);
        res.render('edit',{ user: user, errors: errors.array(),values: req.body });
        return;
    }
    var user =await User.findByIdAndUpdate(id,req.body);
    console.log(user);
    res.redirect('/users');
    //res.send("okokok");
})

//insertUser


router.post("/", [
    check('Name', 'Name require ').not().isEmpty(),
    check('Email').not().isEmpty().withMessage('E-mail require')
        .isEmail().withMessage('Email not Email'),
    check('Birthday', 'Invalid birthday').isISO8601('yyyy-mm-dd'),
    check('Tel', 'Tel more than 10 degits').isLength({ min: 10 }),
    check('Role', 'Role does not Empty').not().isEmpty(),
    check('Email').custom(value => {
        var useruse=[];
        return User.find({ 'Email': value}).then(user =>{
            //console.log(user)
            if (user.length>0) {
                return Promise.reject('E-mail already in use');
            }

        })
    })
  ], async (req, res) => {
    const errors = validationResult(req);
    //console.log(errors);
    if (!errors.isEmpty()) {
        console.log(errors);
        res.render('create',{ errors: errors.array(),values: req.body });
        return;
    }
    const user = new User(req.body);
    await user.save();
    res.redirect('/api/users');
});

//deleteUser
router.delete("/:id", async (req,res)=> {
    const { id } = req.params;
    console.log(id);
    await User.findOneAndDelete({ _id: id });
    //res.redirect('/api/users/');
    res.send("da xoa");
});



  module.exports = router;